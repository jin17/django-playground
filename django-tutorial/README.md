# Django Tutorials

[[_TOC_]]

## Setup

### Django Structure

* Treat Django as just another python package (e.g. `pip install django`).
* Create a project with `django-admin startproject [name]`.
  * Project container can later be renamed (e.g. this `tutorial-project`).
* Create an application with `python manage.py startapp [name]`.
  * Represents a unit of application (blog system, database of some records, polling app, etc.),  
    while project represents a collection of configs and apps for a particular website.
  * Many-to-many relationship with projects.
  * Can be anywhere in python path.
* Add an app to a project in `settings.py::INSTALLED_APPS`.

### Apache

* See for example: [Digital Ocean's tutorial on how to install apache on Debian 10](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-debian-10)
* Use `mod_wsgi-standalone` for now (separate from system's apache installation, self-contained).
* For errors with `bdist_wheel`, try `pip install wheel`.

### Database

* Use SQLite (default) for prototyping.
* See documentations for other backends (e.g. required bindings, adding user with create permission to `settings.py`).

## Development

* Run the development server with `python manage.py runserver` (project's `manage.py`).
  * Check deployment guides for actual production environment.
* Automatically reloads python codes (except actions like adding files which will require a restart).

### Testing

> When testing, more is better.

* Run tests with `python manage.py test <appname>`.
* Django tests (`django.test`) extends the standard python [`unittest` framework](https://docs.python.org/3/library/unittest.html).
  * Be aware of the usual [`unittest` `assert`s](https://docs.python.org/3/library/unittest.html#assert-methods) as well as django's [additional `assert`s](https://docs.djangoproject.com/en/3.1/topics/testing/tools/#assertions).
* The testing system will automatically find tests in any file starting with `test` in the app root directory.
  * Or `python manage.py test <appname>/sub-test-dir`, or check stand ways to organise these tests into different files.

#### Rule-of-Thumb

* Separate `TestClass` for each model and view.
* Separate test method for each set of conditions.
* Name test methods according to the function they are testing.
* Mock any external API calls or any random factors to return specific values appropriate for the test.

#### Test Client

* View-level user simulation.
* Use `django.test.Client` to simulate user interaction and test the views.
  ```python
  from django.test import Client
  from django.urls import reverse

  client = Client()
  response = client.get(reverse('<appname>:<pathname>'))
  # test on attributes like response.status_code, response.content, response.context[..]
  ```
  * Use `django.test.utils.setup_test_environment` when in the shell environment.

### Packaging

* Django apps can be organized into python packages ([example workflow](https://docs.djangoproject.com/en/3.1/intro/reusable-apps/#packaging-your-app)).
* Packaged apps can be installed and imported just like any other python packages.
  ```
  # In your project environment:
  pip install django-appname

  # In project settings.py:
  INSTALLED_APPS = [
      ...
      'appname',
  ]
  ```

## Django Components


### URL Dispatcher

* URLconf (`urls.py`s) is a mapping from paths to views (`views.py`).
  * Includes mapping to other urlconfs (read: apps) for composition (e.g. `path('fun_polls/', include('pollsapp.urls'))`).  
    In this case, "fun_pulls/" will be stripped off before sending the remaining path to the app.
  * Admin views is a special case: `admin.site.urls`.
  * Path names will be used in other parts of the django ecosystem (e.g. when linking to other paths in a template).
* URL patterns maps captured parts of the url to keyword arguments in the corresponding view.
  ```python
  # urls.py
  path('<int:question_id>'/vote/, views.vote)

  # views.py
  def vote(request, question_id):
  ```
* Should be namespaced to avoid ambiguity within templates:
  ```python
  app_name = 'appname'
  urlpatterns = [..]
  ```

### View

<img src="./docs/view-overview.png" width="500px" alt="Diagram of the components related to django view." />

* A view is a mapping from http requests to responses (`HttpResponse`, `Http404`, etc.).
* Load a template with:  
  ```python
  from django.http import HttpResponse
  from django.template import loader

  def someview(request):
      template = loader.get_template('appname/filename.html')
      context = {..}
      return HttpResponse(template.render(context, request))
  ```
  * Template context is just a dictionary that the view can pass information along to the template.
  * This program flow has a shortcut:  
    ```python
    from django.shortcuts import render

    def someview(request):
        context = {..}
        return render(request, 'appname/filename.html', context)
    ```
* For 404s, raise `Http404`, e.g.
  ```python
  from django.http import Http404

  def someview(request, some_id):
      try:
          record = Record.objects.get(pk=some_id)
      except Record.DoesNotExist:
          raise Http404("Record does not exist")
      # ..
  ```
  * This program flow also has a shortcut:
    ```python
    from django.shortcuts import get_object_or_404
    
    def someview(request, some_id):
        record = get_object_or_404(Record, pk=some_id)
        # ..
    ```

#### POST

* Access using the request parameter (e.g. `request.POST['choice']`).
  * Raises `KeyError` if the key wasn't provided.
* View should return a `HttpResponseRedirect` upon a successful POST.
* Use `reverse` to refer to target urls via their path names:
  ```python
  from django.http import HttpResponseRedirect
  from django.urls import reverse

  # ..
  return HttpResponseRedirect(reverse('appname:resultpathname'))
  ```
* For race conditions, see [`F()`](https://docs.djangoproject.com/en/3.1/ref/models/expressions/#avoiding-race-conditions-using-f).

#### Generic Views

* Just another set of shortcuts for common use cases.
* `ListView` displays a list of model objects:
  ```python
  from django.views import generic

  class SomeView(generic.ListView):
      # default: <appname>/<modelname>_list.html
      template_name = 'appname/somepage.html'
      # default: <modelname>_list
      # name of the list that will be accessible from the template
      context_object_name = 'some_list'

      def get_queryset(self):
        # the list that will be given as context to the template
        return somequeryset
  ```
* `DetailView` displays the detail of a particular object:
  ```python
  class SomeView(generic.DetailView):
      model = SomeModel
      # default: <appname>/<modelname>_detail.html
      template_name = 'appname/somepage.html'
  ```
  * For `DetailView`s, context of the object in question will be given to the template automatically.
  * Expects the model key from 'pk' capture (e.g. as opposed to `question_id` earlier in the tutorial).

### Template

* First `templates` for lookup, inner `templates/appname` for namespacing (e.g. `polls/templates/polls/`).
* When linking to other urls in the system, make use of the path names and their namespaces (e.g. `{% url 'appname:pathname' %}`).
* When writing POST forms, use `{% csrf_token %}`.

### Static Files

* Namespace like the templates (e.g. `appname/static/appname/`).
* Add `{% load static %}` to use them in templates.

### Model

> Model is the single, definitive source of truth about your data.

<img src="./docs/model-overview.png" width="800px" alt="Diagram of the components related to django model." />

* Models are represented by python classes (`models.py`).
  * Field definitions translate naturally to and from SQL (independent of backend choice).
* Translate models to SQL with `python manage.py makemigrations [app-name]` ("migration").
  * Migrations store model's changes over time (better for version control).
  * Inspect generated SQL with `python manage.py sqlmigrate [app-name] [migration-#]` (dependent on backend choice).
* Actually create the databases with `python manage.py migrate`.
* Comes with [database API](https://docs.djangoproject.com/en/3.1/topics/db/queries/) for free (can play around in `python manage.py shell`).
* Register a model to the admin site with `admin.site.register(MyModelClass)` in `admin.py`.

#### Model Workflow

1. Make model changes
2. `makemigrations`
3. `migrate`

### Admin Site

* Create new admin user with `python manage.py createsuperuser`.
* Register a model with `admin.site.register(ModelName)`.
* Modify how the model is displayed on a particular admin page by subclassing [`ModelAdmin`](https://docs.djangoproject.com/en/3.1/ref/contrib/admin/#modeladmin-objects).
  * Be sure to also indicate the `ModelAdmin` class when registering with `admin.site.register(ModelName, ModelNameAdmin)`.
* Modify how the model would behave if it were to be put into an admin page by specifying attributes (e.g. `some_model_method.admin_order_field = 'some_model_field'`).
* Related models can be inlined with another model instead of registering them as another separate admin page.
  ```python
  class ChoiceInline(admin.TabularInline):
      model = Choice
      extra = 3

  class QuestionAdmin(admin.ModelAdmin):
      inlines = [ChoiceInline]

  admin.site.register(Question, QuestionAdmin)
  ```
