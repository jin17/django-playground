# Django Playground

[[_TOC_]]

## Django Tutorial

### Setup

```shell
cd django-tutorial
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
pip install ../django-polls
python manage.py migrate
```

### Run

```shell
cd django-tutorial
source venv/bin/activate
python manage.py runserver
```

Check `localhost:8000/polls/`.

### Test

```shell
# Locally
python manage.py test polls
# In a docker container
make test
```

## Docker Tutorial

### Run

```shell
cd docker-tutorial
docker-compose up
```

Check `localhost:8000/`.

### Troubleshoot

#### Running Docker without `sudo`

Add user to the `docker` group (automatically created by the installation):

```shell
sudo usermod -aG docker ${USER}
```

Verify with `id -nG`. Also see this [DigitalOcean tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-debian-10#step-2-%E2%80%94-executing-the-docker-command-without-sudo-(optional)) for details.

#### `:80 bind: address already in use` Error

Make sure the port is available.  
In this case, it's probably apache, so try `sudo systemctl stop apache2` and try again.


## Oscar Tutorial

### Setup

```shell
cd oscar-tutorial
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python manage.py migrate
```

### Run

```shell
cd oscar-tutorial
source venv/bin/activate
python manage.py runserver
```

Check `localhost:8000/`.

### Debugging in Visual Studio Code

Add the appropriate launch configuration in the workspace's `.vscode/launch.json` file, e.g.:

```json
// ...
"configurations": [
    // ...
    {
        "name": "Oscar Sandbox Tutorial",
        "type": "python",
        "request": "launch",
        "python": "${workspaceFolder}/path/to/venv/bin/python",
        "program": "${workspaceFolder}/path/to/sandbox/manage.py",
        "args": [
            "runserver",
        ],
        "django": true,
        "justMyCode": false,
    }
]
```

### Troubleshoot

#### [The Sandbox Tutorial](https://django-oscar.readthedocs.io/en/stable/internals/sandbox.html)

- `make sandbox` requires `npm` (e.g. see [nodejs distribution](https://github.com/nodesource/distributions)).
- Consider checking out a release branch:
  ```shell
  # See latest releases
  git branch --all
  git checkout --track origin/release/x.y
  # Then make sandbox
  ```

#### `NullBooleanField` is Deprecated Warning

See for example [this link](https://www.mslinn.com/blog/2021/02/11/setting-up-django-oscar.html#warn).

```python
# In venv/lib/python3.X/site-packages/oscar/apps/catalogue/abstract_models.py
# Change this line:
value_boolean = models.NullBooleanField(_('Boolean'), blank=True, db_index=True)
# To
value_boolean = models.BooleanField(_('Boolean'), blank=True, null=True, db_index=True)
```
