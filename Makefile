DOCKER_COMPOSE_FILE := docker-test/docker-compose.yml
DOCKER_WEB_SERVICE := web
DJANGO_MANAGER := django-tutorial/manage.py

build:
	docker-compose --file $(DOCKER_COMPOSE_FILE) build
start:
	docker-compose --file $(DOCKER_COMPOSE_FILE) up
test: build test-polls
test-polls:
	docker-compose --file $(DOCKER_COMPOSE_FILE) run --rm $(DOCKER_WEB_SERVICE) $(DJANGO_MANAGER) test polls 
