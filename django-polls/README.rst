=====
Polls
=====

Polls is a django app to conduct web-based polls.
For each question, visitors can choose between a fixed number of answers.

Detailed documentation is in the "docs" directory.

Quick Start
-----------

1. Add "polls" to your INSTALL_APPS setting::

   INSTALLED_APPS = [
      ...
      'polls',
   ]

2. Inlcude the polls URLconf in your project urls.py::

   path('fun_polls/', include('polls.urls')),

3. Run ``python manage.py migrate`` to create the polls models.

4. Start the development server and visit http://127.0.0.1:8000/admin/ to create
   a poll (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/fun_polls/ to participate in the poll.
