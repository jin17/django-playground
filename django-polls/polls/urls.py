from django.urls import path

from . import views


# Set the application namespace.
# E.g. for {% url %} pointing to path named 'detail',
#      we do {% url 'polls:detail' %} instead.
app_name = 'polls'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
