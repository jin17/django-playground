from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import Http404
from django.urls import reverse
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.views import generic
from django.utils import timezone

from .models import Question
from .models import Choice


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_questions'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.filter(
            publication_date__lte=timezone.now()
        ).order_by('-publication_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset(self):
        """Excludes any questions that aren't published yet."""
        return Question.objects.filter(publication_date__lte=timezone.now())


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def vote(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404(f"Question {question_id} does not exist")
    try:
        choice_id = request.POST['choice']
        selected_choice = question.choice_set.get(pk=choice_id)
    except KeyError:
        context = {
            'question': question,
            'error_message': "You didn't select a choice.",
        }
        response = render(request, 'polls/detail.html', context)
    except Choice.DoesNotExist:
        context = {
            'question': question,
            'error_message': f"Choice {choice_id} does not exist.",
        }
        response = render(request, 'polls/detail.html', context)
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return HttpResponseRedirect after a successful POST
        response = HttpResponseRedirect(
            reverse('polls:results', args=(question.id,)))
    return response
