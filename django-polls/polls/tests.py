import datetime

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from .models import Question


def create_question(text, days_offset):
    """
    Create a question with the given `text` and published the given number of
    `days_offset` offset to now (negative for past questions, positive for
    future questions).
    """
    time = timezone.now() + datetime.timedelta(days=days_offset)
    return Question.objects.create(text=text, publication_date=time)


class QuestionIndexViewTests(TestCase):

    def test_no_questions(self):
        """
        If no questions exist, an appropriate message should be displayed.
        """
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context['latest_questions'], [])

    def test_past_question(self):
        """
        Questions with a publication date in the past should be displayed on the
        index page.
        """
        create_question(text="Past question", days_offset=-30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_questions'],
            ['<Question: Past question>']
        )

    def test_future_question(self):
        """
        Questions with a publication date in the future shouldn't be displayed
        on the index page.
        """
        create_question(text="Future question", days_offset=30)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context['latest_questions'], [])

    def test_future_and_past_questions(self):
        """
        When both past and future questions exist, only the past questions
        should be displayed.
        """
        create_question(text="Past question", days_offset=-30)
        create_question(text="Future question", days_offset=30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_questions'],
            ['<Question: Past question>']
        )

    def test_two_past_questions(self):
        """
        The index page should be able to display multiple questions.
        """
        create_question(text="Past question 1", days_offset=-30)
        create_question(text="Past question 2", days_offset=-5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_questions'],
            ['<Question: Past question 2>', '<Question: Past question 1>']
        )


class QuestionDetailViewTests(TestCase):

    def test_future_question(self):
        """
        The detial view of questions with future publication dates should return
        404.
        """
        future_question = create_question(text="Future question", days_offset=5)
        url = reverse('polls:detail', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        """
        The detail view of questions with past publication dates should display
        the question's text.
        """
        past_question = create_question(text="Past question", days_offset=-5)
        url = reverse('polls:detail', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.text)


class QuestionModelTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        """
        Should return False for questions whose publication date is in the
        future.
        """
        future_time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(publication_date=future_time)
        self.assertFalse(future_question.was_published_recently())

    def test_was_published_recently_with_old_question(self):
        """
        Should return False for questions whose publication date is older than
        1 day.
        """
        old_time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(publication_date=old_time)
        self.assertFalse(old_question.was_published_recently())

    def test_was_published_recently_with_recent_question(self):
        """
        Should return True for questions whose publication date is within the
        last day.
        """
        recent_time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(publication_date=recent_time)
        self.assertTrue(recent_question.was_published_recently())
