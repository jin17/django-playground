from django.contrib import admin

from .models import Question
from .models import Choice


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    search_fields = [
        'text',
    ]
    list_display = (
        'text',
        'publication_date',
        'was_published_recently',
    )
    list_filter = [
        'publication_date',
    ]
    fieldsets = [
        (None, {
            'fields': ['text'],
        }),
        ('Date Information', {
            'fields': ['publication_date'],
            'classes': ['collapse'],
        }),
    ]
    inlines = [ChoiceInline]


admin.site.register(Question, QuestionAdmin)
