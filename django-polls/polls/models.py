import datetime

from django.db import models
from django.utils import timezone


class Question(models.Model):
    text = models.CharField(max_length=200)
    publication_date = models.DateTimeField('Publication Date')

    def was_published_recently(self):
        now = timezone.now()
        recency_threshold = now - datetime.timedelta(days=1)
        was_published = self.publication_date <= now
        is_recent = self.publication_date >= recency_threshold
        return was_published and is_recent
    was_published_recently.admin_order_field = 'publication_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = "Published recently?"

    def __str__(self):
        return self.text


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.text
